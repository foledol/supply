'use strict';

const SupplyChain = require('./supply.chain.js');

let supplyChain = new SupplyChain('test.json', false);
if (!supplyChain.test())
    console.error("test failed");

supplyChain = new SupplyChain('data.json', false);
const states = supplyChain.getStates();
states.forEach(state => {
    console.log(supplyChain.getDescription(state.movement));
})

console.log("done");
