'use strict';

const fs = require('fs');
const moment = require('moment');

const REGULAR = "Regular";

class Movement {
    constructor(uid, date, type, shipment) {
        this.uid = uid;
        this.date = date;
        this.type = type;
        this.shipment = shipment;
    }
}

class SupplyChain {

    constructor(filename, initStocks) {
        let rawdata = fs.readFileSync(filename);
        let data = JSON.parse(rawdata);

        this._expressDelay = 1;
        this._regularDelay = 2;

        this._uid = 1;
        this._stocks = this._init(data.stocks);
        this._network = data.network;
        this._expected = data.expected;
        this._shipments = this._init(data.shipments);
        this._movements = this._initMovements(this._shipments, data.products);
        this._movements = this._movements.sort((a, b) => {
            return a.date - b.date !== 0 ? a.date - b.date : a.type - b.type;
        });
        if (initStocks)
            this._initStocks();
    }

    _init(items) {
        return items.map(item => {
            return {...item, uid: this._uid++}
        });
    }

    _initStocks() {
        this._shipments.forEach(shipment => {
           this._stocks.push({
               depot: shipment.depotFrom,
               product: shipment.product,
               quantity: shipment.quantity,
               expiry: shipment.expiry
           })
        });
    }

    _initMovements(shipments, products) {
        let movements = [];
        shipments.forEach(shipment => {

            // Get the product
            const product = products.find(p => p.label === shipment.product)
            if (!product) {
                this.log(`Shipment ${shipment.uid} : invalid product`);
                return;
            }

            // Get the route
            const route = this._network.find(route =>
                route.depotFrom === shipment.depotFrom &&
                route.depotTo === shipment.depotTo);
            if (!route) {
                this.log(`Shipment ${shipment.uid} : invalid route`);
                return;
            }

            const departure = new Date(shipment.departureDate);
            const delay = product.shipmentType === REGULAR ?
                ('regularDelay' in route ? route.regularDelay : this._regularDelay):
                ('expressDelay' in route ? route.expressDelay : this._expressDelay);
            let arrival = new Date(departure)
            arrival.setDate(arrival.getDate() + delay);

            let expiry = new Date(shipment.expiry);
            if (expiry < departure)
                this.log(`Shipment ${shipment.uid} : product expired before departure`);
            else if (expiry < arrival)
                this.log(`Shipment ${shipment.uid} : product expired before arrival`);

            movements.push(new Movement(this._uid++, departure, 0, shipment));
            movements.push(new Movement(this._uid++, arrival, 1, shipment));
        })
        return movements;
    }

    _updateStocks(stocks, movement) {
        const shipment = movement.shipment;
        // Departure movement : remove the batch from the stock
        if (movement.type === 0) {
            return stocks.filter(stock =>
                stock.depot !== shipment.depotFrom || stock.uid !== shipment.uid
            );
        }

        // Arrival movement : add the batch to the stock
        let newStocks = [...stocks];
        newStocks.push({
            depot: shipment.depotTo,
            product: shipment.product,
            quantity: shipment.quantity,
            expiry: shipment.expiry,
            uid: shipment.uid
        });
        newStocks = newStocks.sort((a, b) => {
            return a.depot === b.depot ?
                new Date(a.expiry).getTime() - new Date(b.expiry).getTime() : a.depot.localeCompare(b.depot);
        });
        return newStocks;
    }

    getDescription(movement) {
        const shipment = movement.shipment;
        let date = moment(new Date(movement.date)).format("YYYY-MM-DD");
        if (movement.type === 0)
            return `${date} departure of batch ${shipment.uid} from ${shipment.depotFrom}`
        return `${date} arrival of batch ${shipment.uid} to ${shipment.depotTo}`
    }

    getStates() {
        let stocks = this._stocks;
        return this._movements.map(movement => {
            stocks = this._updateStocks(stocks, movement)
            return {
                movement: movement,
                stocks: stocks,
            }
        })
    }

    log(message) {
        console.log(message);
    }

    test() {
        let result = JSON.stringify(this.getStates());
        return result === JSON.stringify(this._expected);
    }
}

module.exports = SupplyChain
