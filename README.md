# Supply Chain

The purpose of this application is to help the supply manager to predict the stocks that will be available after each shipment. 

## Requirements

- Moment 2.24.0

## Installation

Clone the git repository

```
git clone git@gitlab.com:foledol/supply.git
```

## Usage

To run the application

```
node main
```

## Notes

This README file will be improved later
